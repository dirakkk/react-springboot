1. Gestion des paquets node :
    npm install

2. Build et exécution :
    npm run spring-boot

3. curl
    http://localhost:8093/api/personnes

    CREATE
    curl -X POST -H "Content-Type:application/json" -d '{ "prenom" : "Charles", "nom" : "Fourier" }' http://localhost:8093/api/personnes

    READ
    curl -X GET -H "Content-Type:application/json" http://localhost:8093/api/personnes/1

    UPDATE
    curl -X PUT -H "Content-Type:application/json" -d '{"prenom" : "Jean", "nom" : "Fourier" }' http://localhost:8093/api/personnes/1

    DELETE
    curl -X DELETE -H "Content-Type:application/json" http://localhost:8093/api/personnes/1
