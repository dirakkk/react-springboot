import React from 'react';
import ConnectedEmployeeTable from './employeeTable.jsx'
import ConnectedPostman from './postman.jsx'

export default class Demo  extends React.Component {
  render(){
  	return (
  	<div>
  	    <ConnectedPostman/>
    	<ConnectedEmployeeTable/>
   	</div>
   	)
  }
}