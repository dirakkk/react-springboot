const initialState = [
 /*     {
        nom:"serra",
        prenom:"yannick",
        _links:{
          self:{
            href:"http://localhost:8093/api/personnes/0"
          }
        }
      }*/
    ]

function crudReducer(state = {employees:initialState}, action) {
  console.log("action"+JSON.stringify(action));
  switch (action.type) {
    case 'LOAD_ALL_EMPLOYEES_FULFILLED':
      return Object.assign({}, state, {
        employees: [
          ...action.employees
        ]
      })
    case 'ADD_EMPLOYEE_FULFILLED':
      return Object.assign({}, state, {
        employees: [
          ...state.employees,
          {
            nom: action.nom,
            prenom: action.prenom,
            _links:action._links
          }
        ]
      })
    case 'REMOVE_EMPLOYEE_FULFILLED':
      const employeesList=state.employees.filter((employee)=>{
        return employee._links.href !== action._links
      })
      return ({employees:employeesList})
    
    case 'MODIFY_EMPLOYEE_FULFILLED':
    const index = state.employees.findIndex((item) => item._links.personne.href === action._links.personne.href);
    console.log("index"+index);
    return Object.assign({}, state, {
        employees: [
            ...state.employees.slice(0, index),
            Object.assign({}, state.employees[index], {
            nom: action.nom,
            prenom: action.prenom,
            _links:action._links
          }),
            ...state.employees.slice(index + 1)
        ]
    });
    default:
      return state
  }
}

export default crudReducer