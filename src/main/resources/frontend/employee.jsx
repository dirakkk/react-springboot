import React from 'react';
import $ from 'jquery';
import {removeEmployee,modifyEmployee} from './actions'
import {connect} from 'react-redux'
/* classe qui affiche un employé */
export  class Employee extends React.Component {

	constructor (props) {
		super(props)
		console.log("ds employee"+JSON.stringify(props.employee));
		this.state=({readonly:true,cancelModif:false})
		this.href=props.employee._links.href ;//"http://localhost:8093/api/personnes/1"
	}

    render() {

        return (
            <tr>
                <td><input ref="prenom" type="text" readOnly={this.state.readonly} defaultValue={this.props.employee.prenom} /></td>
                <td><input ref="nom" type="text" readOnly={this.state.readonly} defaultValue={this.props.employee.nom}/></td>
                
                { this.state.cancelModif ?
                	<td><button type="button" onClick={this.onUpdate.bind(this)}>Update</button></td>
                	: <td><button type="button" onClick={this.onModify.bind(this)}>Modify</button></td>
                }  
                { this.state.cancelModif ?
                	<td><button type="button" onClick={this.onCancel.bind(this)}>Cancel</button></td>
                	: <td><button type="button" onClick={this.onDelete.bind(this)}>Delete</button></td>
                }              
                
            </tr>
        );
    }


    onCancel () {
    	this.setState({readonly:false,cancelModif:true})
    }
	
	onUpdate () {
		console.log("update"+this.href);
		const data={"prenom": this.refs.prenom.value,"nom":this.refs.nom.value,_links:this.href}
		this.props.onDispatchModify(data)
    	this.setState({readonly:false,cancelModif:false})
    }

    onModify () {
    	this.setState({readonly:false,cancelModif:true})
    }

 	onDelete () {
 		console.log("delete"+this.href);
 		this.props.onDispatchDelete(this.href)
 	}   
}

const mapDispatchToProps = (dispatch) => {
    console.log("in mapDispatchToProps");
  return {
    onDispatchDelete: (link) => {
      console.log("onDispatchDelete"+JSON.stringify(link)); 
      dispatch(removeEmployee(link))
    },onDispatchModify: (employee) => {
      console.log("onDispatchModify"+JSON.stringify(employee)); 
      dispatch(modifyEmployee(employee))
    }
  }
}

const ConnectedEmployee= connect(null,mapDispatchToProps)(Employee)

export default ConnectedEmployee