import React from 'react';
import {render} from 'react-dom';
import Demo from './demo.jsx';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware} from 'redux'
import crudReducer from './crudReducer.js'
import addEmployeeEpicMiddleware from './actions/epics'

let store = createStore(crudReducer,applyMiddleware(addEmployeeEpicMiddleware))

/* Point d'entrée front */
render(
	<Provider store={store}>
	<Demo/>
	</Provider>,
	 document.getElementById('app')
);
