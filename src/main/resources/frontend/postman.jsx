import React from 'react';
import $ from 'jquery';
import {connect } from 'react-redux'
import {addEmployee} from './actions'

export  class PostMan extends React.Component {
  render(){

    return (
    <div>
    	<div className="row">
          <div className="col-xs-offset-1 col-xs-1"><h4>Prenom:</h4></div>
          <div className="col-xs-offset-3 col-xs-1"><h4>Nom:</h4></div>
      </div>
      <div className="row">    
          <div className="col-xs-offset-1 col-xs-1"><input ref="prenom" type="text"/></div>
          <div className="col-xs-offset-3 col-xs-1"><input ref="nom" type="text"/></div>    
          <div className="col-xs-offset-1 col-xs-2">
            <button 
              type="button" 
              className="btn btn-danger" 
              onClick={this.createMan.bind(this)}>Fire
              </button>
          </div>
      </div> 
    </div>
    )
  }

  createMan ( )  {
  	const data={"prenom": this.refs.prenom.value,"nom":this.refs.nom.value}
    this.props.onFireClick(data)
    this.clean()    
  }

  clean (){
    this.refs.prenom.value="";
    this.refs.nom.value="";
  }
}

const mapStateToProps = (state) => {
  return {
    employees: state.employees
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onFireClick: (employee) => {
      dispatch(addEmployee(employee))
    }
  }
}

const ConnectedPostman= connect(mapStateToProps,mapDispatchToProps)(PostMan)

export default ConnectedPostman