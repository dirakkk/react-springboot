export const addEmployee = (employee) => {
  return {
    type: 'ADD_EMPLOYEE',
    nom:employee.nom,
    prenom:employee.prenom
  }
}


export const loadAllEmployees = (searchPattern) => {
  return {
    type: 'LOAD_ALL_EMPLOYEES',
    searchPattern
  }
}


export const removeEmployee = (link) => {
  return {
    type: 'REMOVE_EMPLOYEE',
    _links:link
  }
}

export const modifyEmployee = (employee) => {
  return {
    type: 'MODIFY_EMPLOYEE',
    _links:employee._links,
     nom:employee.nom,
    prenom:employee.prenom
  }
}