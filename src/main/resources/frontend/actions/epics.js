import { createEpicMiddleware , combineEpics} from 'redux-observable';
import {mergeMap,filter,combineLatest} from 'rxjs'
import { ajax } from 'rxjs/observable/dom/ajax';


const employeeURI='http://localhost:8080/personne/'
const employeeSearchURI='http://localhost:8080/personnes/'

/* http://localhost:8093/api/personnes'*/

const convertItemFromSearch=(item)=>{
	return(
		{
			nom:item._source.nom,
			prenom:item._source.prenom,
			_links:item.links[0]
		}
	)
}


//Load all or search
const loadAllEmployeesEpic = action$ => 
	action$.filter((action)=> action.type === "LOAD_ALL_EMPLOYEES" )
	.mergeMap(action=>ajax.get(employeeSearchURI+action.searchPattern))
	.map(response => {
		console.log("loadAllEmployeesEpic employees:"+JSON.stringify(response.response));
		const responseInShape=response.response.map((item => convertItemFromSearch(item)))
		console.log("employees:"+JSON.stringify(responseInShape));
		return ({ type: "LOAD_ALL_EMPLOYEES_FULFILLED",employees:responseInShape})
	})

let actionPayload=""

//CREATE
const addEmployeeEpic = action$ =>
	action$.filter((action)=>action.type === "ADD_EMPLOYEE" )
	.mergeMap( action => {
		actionPayload=action
		return (ajax.post(employeeURI,{"prenom": action.prenom,"nom":action.nom},{"Content-Type":"application/json"}))
	})
	.map(response => { 
		console.log(JSON.stringify(response)) 
		return addEmployeeFulfilled(actionPayload,response)
	})

const addEmployeeFulfilled = (payload,response) => ({ type: "ADD_EMPLOYEE_FULFILLED",nom:payload.nom, prenom:payload.prenom,_links:response.response.links[0] });

//DELETE
let linksToDelete= "wazza"

const deleteEmployeeEpic = (action$) => { 
	console.log("deleteemployeeepic"+JSON.stringify(action$));
	return (
		action$.filter((action)=>action.type === "REMOVE_EMPLOYEE" )
		.mergeMap( (action) => {
			linksToDelete=action._links;
			return (ajax.delete(action._links,{"Content-Type":"application/json"}))
		})
		.map(response => deleteEmployeeFulfilled(linksToDelete))
	)
};

const deleteEmployeeFulfilled = response => {
	console.log("deleteemployeefullfilled"+response);
	return ({ type: "REMOVE_EMPLOYEE_FULFILLED",_links:response });
}

//UPDATE
const modifyEmployeeEpic = action$ =>
	action$.filter((action)=>action.type === "MODIFY_EMPLOYEE" )
	.mergeMap( action => ajax.put(action._links,{"prenom": action.prenom,"nom":action.nom},{"Content-Type":"application/json"}))
	.map(response => modifyEmployeeFulfilled(response));

const modifyEmployeeFulfilled = payload => ({ type: "MODIFY_EMPLOYEE_FULFILLED",nom:payload.response.nom, prenom:payload.response.prenom,_links:payload.response._links });





const addEmployeeEpicMiddleware = createEpicMiddleware (combineEpics(addEmployeeEpic,deleteEmployeeEpic,modifyEmployeeEpic,loadAllEmployeesEpic))
export default addEmployeeEpicMiddleware
