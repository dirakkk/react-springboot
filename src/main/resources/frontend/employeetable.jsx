import React from 'react';
import ConnectedEmployee from './employee.jsx';
import $ from 'jquery';
import { connect } from 'react-redux'
import { loadAllEmployees } from './actions'

/* classe qui représente un ensemble d'employés */
export  class EmployeeTable extends React.Component {

    componentDidMount () {
        this.props.onDispatchLoadAllEmployees("")
    }

    render() {
        console.log(this.props);
        var rows = [];
        this.props.employees.forEach(function(employee,it) {
            const key=employee._links.href
            rows.push(<ConnectedEmployee employee={employee} key={key}/>);
        });

        return (
            <div className="container">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Prenom</th>
                            <th>Nom</th>
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
                <div class="row">
                    <div class="col-xs-2">Search</div>
                    <div class="col-xs-2"><input ref="search" type="text" defaultValue="" onChange={this.onSearch.bind(this)} /></div>
                </div>
            </div>
        );
    }

    onSearch (){
        console.log("search"+this.href);
        const patternToSearch= this.refs.search.value
        this.props.onDispatchLoadAllEmployees(patternToSearch)
    }
}

const mapStateToProps = (state) => {
    console.log("in mapStateToProps"+JSON.stringify(state));
  return {
    employees: state.employees
  }
}

const mapDispatchToProps = (dispatch) => {
    console.log("in mapDispatchToProps");
  return {
    onDispatchLoadAllEmployees: (patternToSearch) => {
      console.log("onDispatchAllEmployees"); 
      dispatch(loadAllEmployees("*"+patternToSearch+"*"))
    }
  }
}


const ConnectedEmployeeTable = connect(
  mapStateToProps,mapDispatchToProps
)(EmployeeTable)

export default ConnectedEmployeeTable
